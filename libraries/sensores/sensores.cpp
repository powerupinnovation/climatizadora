#include <sensores.h>

Sensores::Sensores()
{
}


bool Sensores::inicializar()
{
    int conta;
    conta = 0;

    Serial.println("inicializando sensores");
	  byte address;
    
    for(address = 1; address < 127; address++ ) 
    {
    
        Wire.beginTransmission(address);
    
        switch (address){
            case PCF8591A_I2C_ADDRESS:
                conta++;
                Serial.println("PCF8591A encontrado");
                break;
            case PCF8591B_I2C_ADDRESS:
                conta++;
                Serial.println("PCF8591B encontrado");
                break;
            case PCF8591C_I2C_ADDRESS:
                conta++;
                Serial.println("PCF8591C encontrado");
                break;
            default:
                break;
        }  

        delay(100);  
    }
     if (conta == 3){
        Serial.println("incializados com sucesso");
        return true;
     }
        else {
             Serial.println("falha na inicialização dos sensores");
             return false;
         }
}

bool Sensores::lertemperaturaS(){

    PCF8591 pcf8591A(PCF8591A_I2C_ADDRESS);
    PCF8591 pcf8591B(PCF8591B_I2C_ADDRESS);

    int conta;
    conta = 0;

    Serial.begin(115200);
    pcf8591A.begin();
    pcf8591B.begin();

    PCF8591::AnalogInput aiA = pcf8591A.analogReadAll();
    Serial.print(aiA.ain0);
    Serial.print(" - ");
    Serial.print(aiA.ain1);
    Serial.print(" - ");
    Serial.print(aiA.ain2);
    Serial.print(" - ");
    Serial.println(aiA.ain3);

    delay(500);

    int ana = pcf8591A.analogRead(AIN0);
    Serial.print("AIN0 --> ");
    Serial.println(ana);

    ana = pcf8591A.analogRead(AIN1);
    Serial.print("AIN1 --> ");
    Serial.println(ana);

    ana = pcf8591A.analogRead(AIN2);
    Serial.print("AIN2 --> ");
    Serial.println(ana);

    ana = pcf8591A.analogRead(AIN3);
    Serial.print("AIN3 --> ");
    Serial.println(ana);

    delay(500);

    PCF8591::AnalogInput aiB = pcf8591B.analogReadAll();
    Serial.print(aiB.ain4);
    Serial.print(" - ");
    Serial.print(aiB.ain5);
    Serial.print(" - ");
    Serial.print(aiB.ain6);
    Serial.print(" - ");
    Serial.println(aiB.ain7);

    delay(500);

    int anaB = pcf8591B.analogRead(AIN4);
    Serial.print("AIN4 --> ");
    Serial.println(anaB);

    anaB = pcf8591B.analogRead(AIN5);
    Serial.print("AIN5 --> ");
    Serial.println(anaB);

    anaB = pcf8591B.analogRead(AIN6);
    Serial.print("AIN6 --> ");
    Serial.println(anaB);

    anaB = pcf8591B.analogRead(AIN7);
    Serial.print("AIN7 --> ");
    Serial.println(anaB);

    delay(500);

    senTMPcB01 = AIN0;
    senTMPcB02 = AIN1;
    senTMPdB01 = AIN2;
    senTMPdB02 = AIN3;
    senTMPdB03 = AIN4;
    senTMPslo = AIN5;
    senTMPdA01 = AIN6;
    senTMPdA02 = AIN7;

    if (senTMPcB01 > 0){
        conta++;
    }
    if (senTMPcB02 > 0){
        conta++;
    }
    if (senTMPdB01 > 0){
        conta++;
    }
    if (senTMPdB02 > 0){
        conta++;
    }
    if (senTMPdB03 > 0){
        conta++;
    }
    if (senTMPslo > 0){
        conta++;
    }
    if (senTMPdA01 > 0){
        conta++;
    }
    if (senTMPdA02 > 0){
        conta++;
    }
    if (conta = 8){
        return true;
    }
      else {
        return false;
      }
}

bool Sensores::leragua()
{

    PCF8591 pcf8591C(PCF8591C_I2C_ADDRESS);

    int contad;
    contad = 0;

    Serial.begin(115200);
    pcf8591C.begin();

    PCF8591::AnalogInput aiC = pcf8591C.analogReadAll();
    Serial.print(aiC.ain8);
    Serial.print(" - ");
    Serial.print(aiC.ain9);
    Serial.print(" - ");
    Serial.print(aiC.ain10);
    Serial.print(" - ");
    Serial.println(aiC.ain11);

    delay(500);

    int anaC = pcf8591C.analogRead(AIN8);
    Serial.print("AIN8 --> ");
    Serial.println(anaC);

    anaC = pcf8591C.analogRead(AIN9);
    Serial.print("AIN9 --> ");
    Serial.println(anaC);

    anaC = pcf8591C.analogRead(AIN10);
    Serial.print("AIN10 --> ");
    Serial.println(anaC);

    anaC = pcf8591C.analogRead(AIN11);
    Serial.print("AIN11 --> ");
    Serial.println(anaC);

    delay(500);

    senNVLvU01 = AIN8;
    senNVLvU02 = AIN9;
    senNVLbA01 = AIN10;
    senNVLbA02 = AIN11;

    return true;

}
bool Sensores::lerumitemp()
{

    #define DHT11A_PIN D4;
    #define DHT11B_PIN D5;
    #define SENUMDSLO D8;

    dht DHT;

    int conta;
    conta = 0;
    int chk;
    chk = 0;

    Serial.begin(115200);

    Serial.print("DHT11A, \t");
    chk = DHT.read11(DHT11A_PIN);
    switch (chk)
    {
    case DHTLIB_OK:  
      Serial.print("OK,\t"); 
      break;
    case DHTLIB_ERROR_CHECKSUM: 
      Serial.print("Checksum error,\t"); 
      break;
    case DHTLIB_ERROR_TIMEOUT: 
      Serial.print("Time out error,\t"); 
      break;
    default: 
      Serial.print("Unknown error,\t"); 
      break;
    }
    // DISPLAY DATA
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(",\t");
    Serial.println(DHT.getTemperature(), 1);

    senUMDcA01 = DHT.getHumidity();
    senTMPcA01 = DHT.getTemperature();

    delay(100);

    Serial.print("DHT11B, \t");
    chk = DHT.read11(DHT11B_PIN);
    switch (chk)
    {
    case DHTLIB_OK:  
      Serial.print("OK,\t"); 
      break;
    case DHTLIB_ERROR_CHECKSUM: 
      Serial.print("Checksum error,\t"); 
      break;
    case DHTLIB_ERROR_TIMEOUT: 
      Serial.print("Time out error,\t"); 
      break;
    default: 
      Serial.print("Unknown error,\t"); 
      break;
    }
    // DISPLAY DATA
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(",\t");
    Serial.println(DHT.getTemperature(), 1);

    senUMDcA02 = DHT.getHumidity();
    senTMPcA02 = DHT.getTemperature();

    delay(100);

    senUMDslo = analogRead(SENUMDSLO);
    Serial.println(senUMDslo);

    if (senUMDcA01 > 0){
        conta++;
    }
      else {
        Serial.println("ERRO LEITURA UMIDADE DHT11A");
      }
    if (senTMPcA01 > 0){
        conta++;
    }
      else{`
        Serial.println("ERRO LEITURA TEMPERATURA DHT11A");
      }
    if (senUMDcA02 > 0){
        conta++;
    }
    else{
      Serial.println("ERRO LEITURA UMIDADE DHT11B");
    }
    if (senTMPcA02 > 0){
        conta++;
    }
    else{
      Serial.println("ERRO TEMPERATURA DHT11B");
    }
    if (senUMDslo > 0){
        conta++;
    }
    else{
      Serial.println("ERRO LEITURA DE UMIDADE SOLO");
    }
     if (conta = 5){
        return true;
    }
      else {
        return false;
        Serial.println("ERRO LEITURA DE UMITEMP");
      }
    
    delay(100);
}

bool Sensores::tmpligado(){

  tempoligado = millis();
  if (tempoligado >= 600000){
    return true
  }
}