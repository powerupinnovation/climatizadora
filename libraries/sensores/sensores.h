#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif
    #include <PCF8591.h>
    #include <Wire.h>
    #include <dht.h>
    #include <maquina_estados.h>

//Main registers
#define PCF8591A_I2C_ADDRESS 0x48
#define PCF8591B_I2C_ADDRESS 0X49
#define PCF8591C_I2C_ADDRESS 0X4A


class Sensores {
    public:
	  Sensores();
      bool inicializar();
      bool lertemperaturaS();
      bool leragua();
      bool lerumitemp();
      bool tmpligado();
    
    private:
    //sensores de tempeeratura
      int senTMPcB01;
      int senTMPcB02;
      int senTMPdB01;
      int senTMPdB02;
      int senTMPdB03;
      int senTMPslo;
      int senTMPdA01;
      int senTMPdA02;
      int senTMPcA01;
      int senTMPcA02;
    //sensores de nível de água
      int senNVLvU01;
      int senNVLvU02;
      int senNVLbA01;
      int senNVLbA02;
    //sensores de umidade
      int senUMDslo;
      int senUMDcA01;
      int senUMDcA02;
    //contagem de tempo
      int tempoligado;

  };