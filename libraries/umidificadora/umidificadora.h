#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif
    #include <sensores.h>
    #include <processos.h>
    #include <ajustetemp.h>
    #include <sensores.h>
    #include <maquina_estados.h>

class Umidificadora {
    public:
	  Umidificadora();
        bool controle();
        int cal_controle();
       
    private:
    int potencia_umid;
    int potencia_insu;
    #define insuflador D6;
    #define umidificadora D5;
};

   