#include <umidificadora.h>

Umidificadora::Umidificadora()
{
    pinMode(D6, OUTPUT);
    pinMode(D5, OUTPUT);
}

bool Umidificadora::controle(){

    analogWrite(insuflador, potencia_insu);
    analogWrite(umidificadora, potencia_umid);

}

int Umidificadora::cal_controle(){

    if (temp.potencia_past = 613 || temp.potencia_past = 511){

        potencia_insu = 1023;

    } else if (temp.potencia_past = 409){

        potencia_insu = 818;
        
    } else if (temp.potencia_past = 307){

        potencia_insu = 613;
        
    } else if (temp.potencia_past = 205){

        potencia_insu = 511;

    } else if (temp.potencia_past = 102){

        potencia_insu = 409;

    } else if (temp.potencia_past = 51){

        potencia_insu = 205;

    }
    
    if(processos.desumi = true){
        if (processos.nvlRSVvap = "Cheio"){
        if (processos.est_umid = 1)
        {
            if (sensores.senUMDcA01 < 85)
            {
                while (sensores.senUMDcA01 > 85)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                
                potencia_umid = 300;
            } else {
                potencia_umid = 100;
            }
            
        } else if (processos.est_umid = 2)
        {
            if (sensores.senUMDcA01 < 73)
            {
                while (sensores.senUMDcA01 > 73)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            }else {
                potencia_umid = 100;
            }

        } else if (processos.est_umid = 3)
        {
            if (sensores.senUMDcA01 < 62)
            {
                while (sensores.senUMDcA01 > 62)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            } else {
                potencia_umid = 100;
            }

        } else if (processos.est_umid = 4)
        {
            if (sensores.senUMDcA01 < 52)
            {
                while (sensores.senUMDcA01 > 52)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            } else {
                potencia_umid = 100;
            }

        } else if (processos.est_umid = 5)
        {
            if (sensores.senUMDcA01 < 44)
            {
                while (sensores.senUMDcA01 > 44)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            } else {
                potencia_umid = 100;
            }

        } else if (processos.est_umid = 6)
        {
            if (sensores.senUMDcA01 < 37)
            {
                while (sensores.senUMDcA01 > 37)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            } else {
                potencia_umid = 100;
            } 

        } else if (processos.est_umid = 7)
        {
            if (sensores.senUMDcA01 < 31)
            {
                while (sensores.senUMDcA01 > 31)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            } else {
                potencia_umid = 100;
            }

        } else if (processos.est_umid = 8)
        {
            if (sensores.senUMDcA01 < 26)
            {
                while (sensores.senUMDcA01 > 26)
                {
                    sensores.senUMDcA01 = sensores.senUMDcA01;
                    potencia_umid = 1000;
                }
                potencia_umid = 300;
                
            } else {
                potencia_umid = 100;
            }

        } else if (processos.est_umid = 9)
        {
            if (sensores.senUMDcA01 < 25)
            {

                sensores.senUMDcA01 = sensores.senUMDcA01;
                potencia_umid = 51;
                
            } else {
                potencia_umid = 0;
            }
        }
        }
    } else if (processos.nvlRSVvap = "Cheio"){
        if (processos.statusUMD = "Alta"){
            potencia_umid = 205;
        } else if (processos.statusUMD = "Baixa"){
            potencia_umid = 1000;
        } else if (processos.statusUMD = "Ideal"){
            potencia_umid = 520;
        }
    } else if (processos.nvlRSVvap = "Intermediario"){
         if (processos.statusUMD = "Alta"){
            potencia_umid = 105;
        } else if (processos.statusUMD = "Baixa"){
            potencia_umid = 600;
        } else if (processos.statusUMD = "Ideal"){
            potencia_umid = 250;
        }
    }
    
}