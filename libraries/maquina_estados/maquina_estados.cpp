#include <maquina_estados.h>


MaquinaEstados::MaquinaEstados()
{
  status = INICIALIZA;
}

void MaquinaEstados::executar()
{
    switch (status)
    {
    case INICIALIZA:
        inicializar();
        break;
    case LEITURA_TEMPERATURA:
        lertemperatura();        
        break;
    case AJUSTA_TEMPERATURA:
        ajustartemperatura();
        break;
    case LEITURA_AGUA:
        leituraagua();
        break;
    case ATUADORES:
        atuador();
        break;
    case UMIDIFICADORA:
        umidificadora();
        break;
    case LEITURA_UMIDADE:
        lerumidade();
        break;
    case PROCESSAR_DADOS:
        processardados();
        break;
    default:
        break;
    }

}

void MaquinaEstados::inicializar()
{
    Serial.begin(115200);
    Serial.println("Inicializando");
    if (sensores.inicializar())
        status = LEITURA_TEMPERATURA;

}

void MaquinaEstados::ajustartemperatura()
{
    Serial.begin(115200);
    Serial.println("ajustar temperatura");
    if (temp.ajustar())
        status = ATUADORES;

}

void MaquinaEstados::leituraagua()
{
    Serial.begin(115200);
    Serial.println("leitura da agua");
    if (sensores.leragua())
        status = LEITURA_UMIDADE;

}

void MaquinaEstados::atuador()
{
    Serial.begin(115200);
    Serial.println("atuar bomba da agua");
    if (atuadores.ligar_desligar())
        status = UMIDIFICADORA;

}

void MaquinaEstados::lertemperatura()
{
    Serial.begin(115200);
    Serial.println("ler temperatura");
    if (sensores.lertemperatura())
        status = LEITURA_AGUA;
    

}

void MaquinaEstados::umidificadora()
{

    Serial.begin(115200);
    Serial.println("umidificadora ligada");
    if (umid.controle())
        status = INICIALIZA;

}

void MaquinaEstados::lerumidade(){

    Serial.begin(115200);
    Serial.println("ler umidade");
    if (sensores.lerumitemp())
        status = PROCESSAR_DADOS;

}

void MaquinaEstados::processardados(){

    Serial.begin(115200);
    Serial.println("processar dados");
    if (processos.concluir())
        status = AJUSTA_TEMPERATURA;
}