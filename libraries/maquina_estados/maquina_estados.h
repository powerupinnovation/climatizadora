#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif
    #include <sensores.h>
    #include <processos.h>
    #include <ajustetemp.h>
    #include <atuadores.h>
    #include <umidificadora.h>

typedef enum
{
    INICIALIZA,
    LEITURA_TEMPERATURA,
    AJUSTA_TEMPERATURA,
    LEITURA_AGUA,
    ATUADORES,
    UMIDIFICADORA,
    LEITURA_UMIDADE,
    PROCESSAR_DADOS
}ESTADOS;


class MaquinaEstados {
    public:
	  MaquinaEstados();
      void executar();
      void inicializar();
      void lertemperatura();
      void ajustartemperatura();
      void leituraagua();
      void atuador();
      void umidificadora();
      void lerumidade();
      void processardados();
    private:
      int status; 
      Sensores sensores; 
      Processos processos;
      Ajustetemp temp;
      Atuadores atuadores; 
      Umidificadora umid;



  };