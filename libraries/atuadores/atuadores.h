#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif
    #include <sensores.h>
    #include <processos.h>
    #include <pcf8574.h>
    #include <maquina_estados.h>

class Atuadores {
    public:
	  Atuadores();
      bool ligar_desligar();
       
    private:
    #define inv_past 1;
    #define exaustor 2;
    #define umidificador 4;
    #define bmbagua 8;
    int past_stts;
};