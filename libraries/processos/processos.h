#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif
    #include <sensores.h>
    #include <maquina_estados.h>

class Processos {
    public:
	  Processos();
        bool concluir();
        bool temp_ar();
        bool umi_ar();
        bool nvl_agua();
        bool dp_mineral();
        bool desumidificador();
        bool display();
    
    private:
    #define tmpALVvra 25;
    int clima;
    char statusclima;
    #define tmpALVinv 22;
    int tmpALVamb;
    int mpALVusu;
    #define umdMAX 70;
    #define umdMIN 30;
    char statusUMD;
    char nvlRSVvap;
    char nvlRSVagua;
    char statusSOLO;
    #define tmpMINamb 15;
    int tmpALVusu;
    #define umdALV 50;
    bool desumi;
    int est_umid;
    int dispSTTSsensor;
};