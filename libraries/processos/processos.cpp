#include <processos.h>

Processos::Processos()
{
}

bool Processos::concluir(){
    if (temp_ar = true && umi_ar = true && nvl_agua = true && dp_mineral = true && display = true && desumidificador = true ){
        concluir = true;
    }
    
}

bool Processos::temp_ar(){
    Serial.begin(115200);

    if (sensores.senTMPcA01 >= tmpALVvra){
        clima = tmpALVvra;
        statusclima = "Quente";
        Serial.println(statusclima);
    }
    else if (sensores.senTMPcA01 <= tmpALVinv){
        clima = tmpALVinv;
        statusclima = "Frio";
        Serial.println(statusclima);
    }
    else {
        statusclima = "Ameno";
        Serial.println(statusclima);
    }
    
    tmpALVamb = clima;

    if (tmpALVamb != tmpALVusu){
        tmpALVamb = tmpALVusu;
    }
    else{
        tmpALVamb = clima;
    }

    return true;

}

bool Processos::umi_ar(){
    Serial.begin(115200);

    if (sensores.senUMDcA01 > umdMAX){
        statusUMD = "Alta";
        Serial.println(statusUMD);
    }
    else if (sensores.senUMDcA01 < umdMIN){
        statusUMD = "Baixa";
        Serial.println(statusUMD);
    }
    else {
        statusUMD = "Ideal";
        Serial.println(statusUMD);
    }

    return true;
}

bool Processos::nvl_agua(){
    Serial.begin(115200);
    
    //Nível de Água do Vaporizador
    if (sensores.senNVLvU01 > 0) {
        nvlRSVvap = "Cheio";
        Serial.println(nvlRSVvap);
    }
    else if (sensores.senNVLvU02 > 0) {
        nvlRSVvap = "Intermediario";
        Serial.println(nvlRSVvap);
    }
    else {
        nvlRSVvap = "Vazio";
        Serial.println(nvlRSVvap);
    }


    //Nível do reservatório de Água
    if (sensores.senNVLbA01 > 0 ){
        nvlRSVagua = "Cheio";
        Serial.println(nvlRSVagua);
    }
    else if (sensores.senNVLbA02 > 0) {
        nvlRSVagua = "Intermediario";
        Serial.println(nvlRSVagua);
    }
    else {
        nvlRSVagua = "Vazio";
        Serial.println(nvlRSVagua);
    }

        return true;
}

bool Processos::dp_mineral(){
    Serial.begin(115200);

    if (sensores.senUMDslo == 0 && sensores.senTMPslo > (sensores.senTMPcA01 + 10)){
        statusSOLO = "Seco Quente";
        Serial.println(statusSOLO);
    }
    else if (sensores.senUMDslo > 0 && sensores.senTMPslo > (sensores.senTMPcA01 + 10)){
        statusSOLO = "Úmido Quente";
        Serial.println(statusSOLO);
    }
    else if (sensores.senUMDslo == 0 && sensores.senTMPslo < sensores.senTMPcA01){
        statusSOLO = "Seco Frio";
        Serial.println(statusSOLO);
    }
    else if (sensores.senUMDslo > 0 && sensores.senTMPslo < sensores.senTMPcA01){
        statusSOLO = "Úmido Frio";
        Serial.println(statusSOLO);
    }
    
    return true;
}

bool Processos::display(){
    if (nvlRSVagua = "Cheio"){
        dispSTTSsensor = 1;
    }
    if (statusUMD = "Baixa" && nvlRSVvap = "Vazio"){
        dispSTTSsensor = 2;
    }

 return true;
}

bool Processos::desumidificador(){
    Serial.begin(115200);
    desumi = false;

    if (sensores.tmpligado = true){
        if ((statusUMD == "Alta" || sensores.senUMDslo == 0) && nvlRSVagua != "Cheio"){
            while (!desumi)
            {
                sensores.senTMPcA01 = sensores.senTMPcA01;
              if (sensores.senTMPcA01 >= 29){
                  est_umid = 1;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 26 && sensores.senTMPcA01 <= 28){
                  est_umid = 2;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 24 && sensores.senTMPcA01 < 26){
                  est_umid = 3;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 21 && sensores.senTMPcA01 < 24){
                  est_umid = 4;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 18 && sensores.senTMPcA01 < 21){
                  est_umid = 5;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 16 && sensores.senTMPcA01 < 18){
                  est_umid = 6;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 13 && sensores.senTMPcA01 < 16){
                  est_umid = 7;
                  desumi = true;
              }else if (sensores.seTMPcA01 >= 10 && sensores.senTMPcA01 < 13){
                  est_umid = 8;
                  desumi = true;
              }else if (sensores.seTMPcA01 < 10){
                  est_umid = 9;
                  desumi = true;
              }  
            }
            
        }
        
    }

    return true;

}

