#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif
    #include <sensores.h>
    #include <processos.h>
    #include <atuadores.h>
    #include <maquina_estados.h>

class Ajustetemp {
    public:
	  Ajustetemp();
        bool ajustar();
        int calcular_ajuste();
       
    private:
    #define pastilhas D7;
    int diferenca;
    int potencia_past;
    int pot;
    int potencia_pastG;
};
   