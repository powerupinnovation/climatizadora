#include <ajustetemp.h>

Ajustetemp::Ajustetemp()
{
    pinMode(D7, OUTPUT);
}

bool Ajustetemp::ajustar(){

    if (processos.clima > processos.tmpMINamb){
        if (atuadores.past_stts = 1){
            analogWrite(pastilhas, potencia_pastG);
        }
    } else {
        if (atuadores.past_stts = 1){
            analogWrite(pastilhas, potencia_pastG);
        }
    }
} 
int Ajustetemp::calcular_ajuste(){

    diferenca = sensores.senTMPcA01 - sensores.clima;

    if (sensores.clima > processos.tmpMINamb){
        pot = diferenca;
    } else {
        pot = diferenca * (-1);
    }
    
    if (pot >= 5){
        potencia_past = 613;
    } else if (pot >= 3 && pot < 5){
        potencia_past = 511;
    } else if (pot <= 2 && pot < 3){
        potencia_past = 409;
    } else if (pot >= 1 && pot < 2){
        potencia_past = 307;
    } else if (pot >= 0 && pot < 1){
        potencia_past = 205;
    } else if (pot < 0 && pot > (-2)){
        potencia_past = 102;
    } else if (pot < (-2)){
        potencia_past = 51;
    }

    if (potencia_pastG < potencia_past){
        while (potencia_pastG < potencia_past)
        {
            potencia_pastG = potencia_pastG + 51;
            delay(10000);
        }
    } else if (potencia_pastG > potencia_past){
        while (potencia_pastG > potencia_past)
        {
            potencia_pastG = potencia_pastG - 51;
            delay(10000);
        }
        
    }
}