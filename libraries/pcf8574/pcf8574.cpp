#include <Pcf8574.h>
#include <Wire.h>

Pcf8574::Pcf8574()
{
	Wire.begin();
}


byte Pcf8574::readSinglePort(byte port)
{
    Serial.println("ler uma porta");
	byte reading;			
	Wire.beginTransmission(PCF8574_ADD);
	Wire.write(port);      		
	Wire.endTransmission(); 
	Wire.requestFrom(PCF8574_ADD, READ);
	reading = Wire.read();
    Serial.println(reading,HEX); 
	return reading;
}

byte Pcf8574::readallPorts()
{
    Serial.println("ler todas as portas");
	byte reading;			
	Wire.beginTransmission(PCF8574_ADD);
	Wire.write(byte(address + B11000000) ); //+ B11000000      		// sets register pointer to echo #1 register (0x02)
	Wire.endTransmission(); 	
	Wire.requestFrom(PCF8574_ADD, READ);
	while (Wire.available()) { 				// slave may send less than requested
		reading = Wire.read(); 				// receive a byte as character
		Serial.println(reading,HEX);        // print the character
	}
	Wire.endTransmission();  
	return reading;
}


void Pcf8574::writeSinglePort(byte port,byte value)
{
	Serial.println("Escrever em uma porta");
	Wire.beginTransmission(PCF8574_ADD);
    Wire.write(port);            	// sends instruction byte
	Wire.write(value);         		// sends value byte
	Wire.endTransmission();
}

void Pcf8574::writeAllPorts(byte port,byte value)
{
	Serial.println("Escrever em todas as portas");
	Wire.beginTransmission(PCF8574_ADD);
	Wire.write(port);            	// sends instruction byte
	Wire.write(value);         		// sends value byte
	Wire.endTransmission();	
   
}