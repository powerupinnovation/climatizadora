#if defined(ARDUINO) && ARDUINO >= 100
    #include <Arduino.h>
  #else
    #include <WProgram.h>
  #endif

#define PCF8574_ADD  0x38

//Main registers
#define COMMAND 	    0x00
#define P0		    1
#define P1		    2
#define P2	        4
#define P3	        8
#define P4	        16	
#define P5	        32
#define P6          64
#define P7          128
#define READ        0
#define WRITE       1

class Pcf8574 {
    public:
	  Pcf8574();
      byte readSinglePort(byte port);
      byte readallPorts();
      void writeSinglePort(byte port,byte value);
      void writeAllPorts(byte value);
 
    
    private:
      byte currentvalue;      
  };