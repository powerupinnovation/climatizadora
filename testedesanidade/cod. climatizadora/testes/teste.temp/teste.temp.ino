#include <PCF8591.h>
#include "Arduino.h"

#define PCF8591A_I2C_ADDRESS 0x48
#define PCF8591B_I2C_ADDRESS 0X49
#define PCF8591C_I2C_ADDRESS 0X4A

PCF8591 pcf8591A(PCF8591A_I2C_ADDRESS);
PCF8591 pcf8591B(PCF8591B_I2C_ADDRESS);
PCF8591 pcf8591C(PCF8591C_I2C_ADDRESS);

void setup()
{
  Serial.begin(115200);
  pcf8591A.begin();
  pcf8591B.begin();
  pcf8591C.begin();
}

void loop()
{
  PCF8591::AnalogInput aiA = pcf8591A.analogReadAll();
  Serial.print(aiA.ain0);
  Serial.print(" - ");
  Serial.print(aiA.ain1);
  Serial.print(" - ");
  Serial.print(aiA.ain2);
  Serial.print(" - ");
  Serial.println(aiA.ain3);

  delay(500);

  int ana = pcf8591A.analogRead(AIN0);
  Serial.print("AIN0 --> ");
  Serial.println(ana);

  ana = pcf8591A.analogRead(AIN1);
  Serial.print("AIN1 --> ");
  Serial.println(ana);

  ana = pcf8591A.analogRead(AIN2);
  Serial.print("AIN2 --> ");
  Serial.println(ana);

  ana = pcf8591A.analogRead(AIN3);
  Serial.print("AIN3 --> ");
  Serial.println(ana);
  delay(500);

  PCF8591::AnalogInput aiB = pcf8591B.analogReadAll();
  Serial.print(aiB.ain4);
  Serial.print(" - ");
  Serial.print(aiB.ain5);
  Serial.print(" - ");
  Serial.print(aiB.ain6);
  Serial.print(" - ");
  Serial.println(aiB.ain7);

  delay(500);

  int anaB = pcf8591B.analogRead(AIN4);
  Serial.print("AIN4 --> ");
  Serial.println(anaB);

  anaB = pcf8591B.analogRead(AIN5);
  Serial.print("AIN5 --> ");
  Serial.println(anaB);

  anaB = pcf8591B.analogRead(AIN6);
  Serial.print("AIN6 --> ");
  Serial.println(anaB);

  anaB = pcf8591B.analogRead(AIN7);
  Serial.print("AIN7 --> ");
  Serial.println(anaB);
  delay(500);

  PCF8591::AnalogInput aiC = pcf8591C.analogReadAll();
  Serial.print(aiC.ain8);
  Serial.print(" - ");
  Serial.print(aiC.ain9);
  Serial.print(" - ");
  Serial.print(aiC.ain10);
  Serial.print(" - ");
  Serial.println(aiC.ain11);

  delay(500);

  int anaC = pcf8591C.analogRead(AIN8);
  Serial.print("AIN8 --> ");
  Serial.println(anaC);

  anaC = pcf8591C.analogRead(AIN9);
  Serial.print("AIN9 --> ");
  Serial.println(anaC);

  anaC = pcf8591C.analogRead(AIN10);
  Serial.print("AIN10 --> ");
  Serial.println(anaC);

  anaC = pcf8591C.analogRead(AIN11);
  Serial.print("AIN11 --> ");
  Serial.println(anaC);
  delay(3000);
}
