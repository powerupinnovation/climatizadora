#include <pcf8574.h>


Pcf8574 controladora_rele = Pcf8574();

void setup() {
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(600);
  controladora_rele.writeAllPorts(0xff);
  delay(600);
  controladora_rele.writeAllPorts(1);
  delay(600);
  controladora_rele.writeAllPorts(2);
  delay(600);
  controladora_rele.writeAllPorts(4);
  delay(600);
  controladora_rele.writeAllPorts(8);
  delay(600);
  controladora_rele.writeAllPorts(16);
  delay(600);
  controladora_rele.writeAllPorts(32);
  delay(600);
  controladora_rele.writeAllPorts(64);
  delay(600);
  controladora_rele.writeAllPorts(128);
  delay(600);
}
