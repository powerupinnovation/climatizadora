//Teste de sanidade

String inputString;

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  Serial.println("Teste de sanidade");
}

void loop() {
  // put your main code here, to run repeatedly:
  inputString = getserialstring();

  if (inputString.indexOf("teste")>=0)
  {
    Serial.println("ok");
  }

}

String getserialstring()
{
  char inputchar;
  String tmpstring;
  if (Serial.available())
  {
    while(Serial.available())
    {
      inputchar = Serial.read();
      tmpstring.concat(inputchar);
      delay(10);
    }
  }
  return tmpstring;  
}
